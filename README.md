[![License: CC BY-NC-SA 4.0](https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by-nc-sa/4.0/)

# scripts

All kind of useful scripts to automate tasks that you do not want to do by hand. In the documentation of each script you can learn more about the intent and behavior of the script. 
