
# Program description: Resizes the input file(s). It can accept directories 
# and individual images. If a directory is provided the output is a directory
# with '-$size' attached to it along with all the output inside. if a 
# single image is provided '-$size' is attached to it.

# Program arguments: 
#  - $1: directory or file to resize to.
#  - $2: resolution to resize to.

path="$1"
size="$2"

# Function description: prints out a basic progress bar to the console.

# Function arguments: 
#  - $1: maximum number of elements
#  - $2: current element index

progressBar()
{
    n="$1"
    c="$2"

    echo -n "[ "
    for ((i = 0 ; i <= c; i++)); do echo -n "#"; done
    for ((j = c ; j < n; j++)); do echo -n " "; done
    echo -n " ] " 
    echo -n " $c/$n" $'\r'
}

# resize an entire directory
if [[ -d "$path" ]]; then 
    # determine target directory
    directory=`basename "$path"`
    parent="${path%\\*}"
    destination="$parent\\$directory-$size"

    # create output folder if it doesn't exist
    if [ ! -d "$destination" ]; then 
        mkdir "$destination"
    fi

    # progress bar
    c=0
    n=`ls "$path" | wc -l`

    # for each file in the directory
    for entry in "$path/"*;
    do
        # update progress bar
        c=$(($c+1)) 
        progressBar "$n" "$c"

        # determine name of output file
        base=`basename "$entry"`
        target="$destination/$base"

        # check if our output is more recent
        if [[ "$entry" -nt "$target" ]] || [ ! -f "$target" ]; then
            # resize the image
            extension="${entry#*.}"
            magick convert "$entry" -resize "${size}x${size}" "$destination/$base"
        fi
    done

    exit 0
fi

# resize a single file
if [[ -f "$path" ]]; then 

    # get extension, file name and directory
    extension="${path#*.}"
    base=`basename "$path" ".$extension"`
    directory="${path%\\*}"

    # convert it
    magick convert "$path" -resize "${size}x${size}" "$directory/$base-$size.$extension"

    # all went well
    exit 0
fi

# something is wrong
echo "Could not perform resize (to $size) task."
echo "Input of task: path - $path"
echo "Input of task: size - $size"

read 
exit 1

# Script made by (Jip) Willem Wijnia
# Licensed with (CC BY-NC-SA 4.0)
# For more information: https://creativecommons.org/licenses/by-nc-sa/4.0/