
# Program description: Constructs a 2x2 grid of the input file in a single 
# image. It can accept directories and individual images. If a directory is 
# provided the output is a directory with '-append' attached to it along with all 
# the output inside. if a single imge is provided '-append' is attached to it.

# Program arguments: 
#  - $1: directory or file to append to.

path="$1"

# Function description: prints out a basic progress bar to the console.

# Function arguments: 
#  - $1: maximum number of elements
#  - $2: current element index

progressBar()
{
    n="$1"
    c="$2"

    echo -n "[ "
    for ((i = 0 ; i <= c; i++)); do echo -n "#"; done
    for ((j = c ; j < n; j++)); do echo -n " "; done
    echo -n " ] " 
    echo -n " $c/$n" $'\r'
}

if [[ -d "$path" ]]; then 
    # determine target directory
    directory=`basename "$path"`
    parent="${path%\\*}"
    destination="$parent\\$directory-appended"

    # create output folder if it doesn't exist
    if [ ! -d "$destination" ]; then 
        mkdir "$destination"
    fi

    # progress bar
    c=0
    n=`ls "$path" | wc -l`

    # for each file in the directory
    for entry in "$path/"*;
    do
        # update progress bar
        c=$(($c+1)) 
        progressBar "$n" "$c"

        # determine name of output file
        base=`basename "$entry"`
        target="$destination/$base"

        # check if our output is more recent
        if [[ "$entry" -nt "$target" ]] || [ ! -f "$target" ]; then
            # append the image
            extension="${entry#*.}"
            temp="$destination/temp.$extension"
            magick "$entry" "$entry" -append "$temp"
            magick "$temp" "$temp" +append "$destination/$base"
            rm "$temp"
        fi
    done

    exit 0
fi

if [[ -f "$path" ]]; then 

    # get extension, file name and directory
    extension="${path#*.}"
    base=`basename "$path" ".$extension"`
    directory="${path%\\*}"

    # append it
    temp="$directory/temp.$extension"
    magick "$path" "$path" -append "$temp"
    magick "$temp" "$temp" +append "$directory/$base-appended.$extension"
    rm "$temp"

    # all went well
    exit 0

fi

# something is wrong
echo "Could not perform append task."
echo "Input of task: path - $path"

read 
exit 1

# Script made by (Jip) Willem Wijnia
# Licensed with (CC BY-NC-SA 4.0)
# For more information: https://creativecommons.org/licenses/by-nc-sa/4.0/