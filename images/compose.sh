
# Program description: Does a cross product between two folders. The first argument
# should have RGB images in it (with no alpha channel). The second argument should have
# greyscale images in it. Each RGB input will have a variant with one of the greyscale 
# images as its alpha channel.

# documentation: https://imagemagick.org/script/compose.php

# Program arguments: 
#  - $1: directory with RGB images
#  - $2: directory with greyscale images

pathRGB="$1"
pathGrey="$2"

# Function description: prints out a basic progress bar to the console.

# Function arguments: 
#  - $1: maximum number of elements
#  - $2: current element index

progressBar()
{
    n="$1"
    c="$2"

    echo -n "[ "
    for ((i = 0 ; i <= c; i++)); do echo -n "#"; done
    for ((j = c ; j < n; j++)); do echo -n " "; done
    echo -n " ] " 
    echo -n " $c/$n" $'\r'
}

# error checking
if [ -z "$(ls -A "$pathRGB")" ]; then
    echo "There are no RGB textures to process."
    read
fi

# error checking
if [ -z "$(ls -A "$pathGrey")" ]; then
    echo "There are no greyscale textures to process."
    read
fi

# create output folder if it doesn't exist
parent="${pathRGB%\\*}"
baseGrey=`basename "$pathGrey"`
baseRGB=`basename "$pathRGB"`
pathOut="$parent\\$baseRGB-$baseGrey"
if [ ! -d "$pathOut" ]; then 
    mkdir "$pathOut"
fi

# desired extension
ext=".tif"

# progress bar
c=0
k=`ls "$pathRGB" | wc -l`
l=`ls "$pathGrey" | wc -l`
n=$(($k * $l))

for grey in "$pathGrey/"*;
do
    # extract file name
    fileGreyNoExt="${grey%.*}"
    fileGrey=`basename "$fileGreyNoExt"`

    for rgb in "$pathRGB/"*;
    do
        # update progress bar
        c=$(($c+1)) 
        progressBar "$n" "$c"

        # extract file name
        fileRGBNoExt="${rgb%.*}"
        fileRGB=`basename "$fileRGBNoExt"`

        # combine file names
        fileCombined="$fileRGB-$fileGrey$ext"
        fileOutput="$pathOut/$fileCombined"

        # only change if file we want to convert is newer
        if [ ! -f "$fileOutput" ] || [[ "$grey" -nt "$fileOutput" ]] || [[ "$rgb" -nt "$fileOutput" ]]; then
            magick -compose copy-opacity "$rgb" "$grey" -composite "$fileOutput" &>/dev/null
        fi
    done
done

# Script made by (Jip) Willem Wijnia
# Licensed with (CC BY-NC-SA 4.0)
# For more information: https://creativecommons.org/licenses/by-nc-sa/4.0/
