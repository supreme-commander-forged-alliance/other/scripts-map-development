
path="$1"
size="2048"

if [[ -d "$path" ]] || [[ -f "$path" ]]; then 
    echo "Resizing $path to ${size}x${size}"

    # determine location of this script to find the real script
    scriptPath="${0%\\*}"
    "$scriptPath"/resize.sh "$path" "$size"

    exit 0
fi

# something is wrong
echo "Could not perform resize (to $size) task."
echo "Input of task: path - $path"

read 
exit 1