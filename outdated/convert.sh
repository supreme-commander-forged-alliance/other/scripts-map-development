#!/bin/bash

# Variables used throughout the script

resolutionMask=256
resolutionHeightmap=513

target="../kaali/"

# Check / create for target folder structure

if [ ! -d "$target/env" ] ; then 
    mkdir "$target/env"
    echo "Created folder: $target/env"
fi

if [ ! -d "$target/env/decals" ] ; then 
    mkdir "$target/env/decals"
    echo "Created folder: $target/env/decals"
fi 

if [ ! -d "$target/masks" ] ; then 
    mkdir "$target/masks"
    echo "Created folder: $target/masks"
fi

if [ ! -d "$target/heightmaps" ] ; then 
    mkdir "$target/heightmaps"
    echo "Created folder: $target/heightmaps"
fi 

# Check / create for source folder structure

if [ ! -d "output-decals" ] ; then 
    mkdir "output-decals"
    echo "Created folder: output-decals"
fi

if [ ! -d "output-masks" ] ; then 
    mkdir "output-masks"
    echo "Created folder: output-masks"
fi 

if [ ! -d "output-heightmaps" ] ; then 
    mkdir "output-heightmaps"
    echo "Created folder: output-heightmaps"
fi

if [ ! -d "temp" ] ; then 
    mkdir "temp"
    echo "Created folder: temp"
fi

for entry in "output-masks/"*;
do
    # determine new file location
    fileWithoutExtension="${entry%.*}"
    fileNew=`basename $fileWithoutExtension`
    rawNew="$target/masks/$fileNew.raw"

    # only change if file we want to convert is newer
    if [[ "$entry" -nt "$rawNew" ]] || [ ! -f "$rawNew" ]; then
        # convert the file
        echo "Resizing: $entry"
        magick convert "$entry" -resize "${resolutionMask}x${resolutionMask}" "$fileWithoutExtension-resized.png"
        magick convert "$fileWithoutExtension-resized.png" -depth 8 "$fileWithoutExtension-resized.gray"

        # move it to the new location
        rm "$fileWithoutExtension-resized.png"
        mv "$fileWithoutExtension-resized.gray" "$rawNew"
    else
        echo "Skipping: $entry (not updated)"
    fi
done

for entry in "output-heightmaps/"*;
do
    # determine new file location
    fileWithoutExtension="${entry%.*}"
    fileNew=`basename $fileWithoutExtension`
    rawNew="$target/heightmaps/$fileNew.raw"

    # only change if file we want to convert is newer
    if [[ "$entry" -nt "$rawNew" ]] || [ ! -f "$rawNew" ]; then
        # convert the file
        echo "Resizing: $entry"
        magick convert "$entry" -resize "${resolutionHeightmap}x${resolutionHeightmap}" "$fileWithoutExtension-resized.png"
        magick convert "$fileWithoutExtension-resized.png" -depth 16 "$fileWithoutExtension-resized.gray"

        # move it to the new location
        rm "$fileWithoutExtension-resized.png"
        mv "$fileWithoutExtension-resized.gray" "$rawNew"
    else
        echo "Skipping: $entry (not updated)"
    fi
done

for entry in "output/"*;
do
    # determine old file location
    fileOld="${entry%.*}"
    ddsOld="$fileOld.dds"

    # determine new file location
    fileNew=`basename $fileOld`
    ddsNew="$target/env/decals/$fileNew.dds"

    # only change if file we want to convert is newer
    if [[ "$entry" -nt "$ddsNew" ]] || [ ! -f "$ddsNew" ]; then
        # convert the file
        echo "Converting: $entry"
        magick "$entry" -define dds:compression=dxt5 "$ddsOld"

        # move it to the new location
        mv "$ddsOld" "$ddsNew"
    else
        echo "Skipping: $entry (not updated)"
    fi
done