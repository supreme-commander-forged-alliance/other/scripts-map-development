#!/bin/bash

# Program description: replaces each file that has entry $1 in it with $2
# Program arguments: 
#  - $1: the expression that will be replaced
#  - $2: the expression that will take its place

# About making bash functions: https://devqa.io/create-call-bash-functions/
# About the sed functionality: https://linuxhint.com/bash_sed_examples/

# $1 = regular expression for files
# $2 = pattern to dismiss
rename(){
    for fname in $1 ; do echo "$fname" ; done
    for fname in $1 ; do mv "$fname" "$(echo "$fname" | sed $2)" ; done
}

rename "*$1*" "s/$1/$2/"